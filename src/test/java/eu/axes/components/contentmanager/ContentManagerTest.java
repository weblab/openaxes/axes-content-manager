package eu.axes.components.contentmanager;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.content.api.ContentManager;
import org.ow2.weblab.core.extended.exception.WebLabUncheckedException;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Image;
import org.ow2.weblab.core.model.MediaUnit;
import org.purl.dc.elements.DublinCoreAnnotator;

import eu.axes.utils.AxesAnnotator;


/**
 * The unit test class
 *
 * @author ymombrun
 */
@SuppressWarnings({ "static-method", "javadoc" })
public final class ContentManagerTest {



	@Test
	public void testWriteRead() throws Exception {
		Document doc = WebLabResourceFactory.createResource("test", "pom", Document.class);
		new DublinCoreAnnotator(doc).writeFormat("application/xml");
		AxesAnnotator aa = new AxesAnnotator(doc);
		aa.writeCollectionId("cCollectionId");
		aa.writeVideoId("vVideoId");
		final ContentManager manager = ContentManager.getInstance();
		manager.writeNativeContent(new File("pom.xml"), doc);

		final List<String> imagesPath = Arrays.asList("src/test/resources/axesContentManager.properties", "src/test/resources/log4j.properties", "src/test/resources/contentManager.properties",
				"src/test/java/eu/axes/components/contentmanager/ContentManagerTest.java");

		for (final String path : imagesPath) {
			final Image img = WebLabResourceFactory.createAndLinkMediaUnit(doc, Image.class);
			if (path.endsWith("properties")) {
				new DublinCoreAnnotator(img).writeFormat("text/x-java-properties");
			} else {
				new DublinCoreAnnotator(img).writeFormat("text/x-java-source");
			}
			aa = new AxesAnnotator(img);
			aa.writeCollectionId("cCollectionId");
			aa.writeVideoId("vVideoId");
			aa.writeShotId("s" + System.nanoTime());
			aa.writeFrameId("keyframe" + FilenameUtils.getBaseName(path));
			manager.writeNativeContent(new File(path), img);
		}

		new WebLabMarshaller().marshalResource(doc, new File("target/out.xml"));

		File pom = manager.readNativeContent(doc);
		Assert.assertEquals(FileUtils.checksumCRC32(new File("pom.xml")), FileUtils.checksumCRC32(pom));

		for (int i = 0; i < imagesPath.size(); i++) {
			MediaUnit img = doc.getMediaUnit().get(i);
			File imgFile = manager.readNativeContent(img);
			Assert.assertEquals(FileUtils.checksumCRC32(new File(imagesPath.get(i))), FileUtils.checksumCRC32(imgFile));
		}
	}


	@Test(expected = WebLabUncheckedException.class)
	public void testSystemPropAndNotAFolder() throws Exception {
		System.setProperty(AxesContentManager.ROOT_FOLDER, "pom.xml");
		try {
			ContentManager.getInstance();
		} finally {
			System.clearProperty(AxesContentManager.ROOT_FOLDER);
		}
	}


}
