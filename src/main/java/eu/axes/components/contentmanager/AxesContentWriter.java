package eu.axes.components.contentmanager;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.content.api.ContentWriter;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.exception.WebLabNotYetImplementedException;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;

/**
 * This implementation of the ContentWriter enable to store NativeContent files.
 *
 *
 * @author ymombrun
 * @date 2012-05-15
 */
public class AxesContentWriter extends AxesContentManager implements ContentWriter {


	/**
	 * The default constructor
	 */
	public AxesContentWriter() {
		super();
		this.log.debug("AxesContentWriter loaded on " + this.rootFolder.getAbsolutePath() + ".");
	}


	@Override
	public URI writeContent(final InputStream content, final Resource resource) throws WebLabCheckedException {
		if (new WProcessingAnnotator(resource).readNativeContent().hasValue()) {
			final String message = "Only one native content could be annotated on a resource.";
			this.log.error(message);
			throw new WebLabCheckedException(message);
		}

		final String ext = this.getFormatExt(resource);
		return this.writeContent(content, resource, ext);
	}


	/**
	 * @param content
	 *            The input content to be copied
	 * @param resource
	 *            The resource to be used to guess the URI
	 * @param extension
	 *            The extension of the file behind <code>content</code>
	 * @return The new URI of the content stored
	 * @throws WebLabCheckedException
	 *             If an error occurred creating the URI or the file path, or during the copy
	 */
	public URI writeContent(final InputStream content, final Resource resource, final String extension) throws WebLabCheckedException {
		try {
			final URI contentId = this.getContentUri(resource, extension);

			final File f = this.getFilePath(contentId);
			if (f.exists()) {
				this.log.debug("File " + f.getAbsolutePath() + " already exists. No new copy will be done.");
				return contentId;
			}
			try {
				FileUtils.copyInputStreamToFile(content, f);
			} catch (final IOException ioe) {
				final String message = "Unable to write content for resource " + resource.getUri() + " in File " + f.getAbsolutePath() + ".";
				this.log.error(message);
				throw new WebLabCheckedException(message, ioe);
			}
			return contentId;
		} finally {
			IOUtils.closeQuietly(content);
		}
	}


	@Override
	public URI writeExposedContent(final InputStream content, final Resource resource) {
		final String message = "The write Exposed Content method is not yet implemented.";
		LogFactory.getLog(this.getClass()).error(message);
		throw new WebLabNotYetImplementedException(message);
	}

}
