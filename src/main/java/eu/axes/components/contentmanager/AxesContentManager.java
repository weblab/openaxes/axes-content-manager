package eu.axes.components.contentmanager;

import java.io.File;
import java.net.URI;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.exception.WebLabUncheckedException;
import org.ow2.weblab.core.extended.properties.PropertiesLoader;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.rdf.Value;
import org.purl.dc.elements.DublinCoreAnnotator;

import eu.axes.utils.AxesAnnotator;
import eu.axes.utils.NamingSchemeUtils;

/**
 * This class is just a common parent for AxesContentReader and AxesContentWriter.
 *
 * @author ymombrun
 * @date 2012-05-15
 */
public class AxesContentManager {


	protected static final String ROOT_FOLDER = "rootFolder";


	protected static final String PROPERTY_FILE = "axesContentManager.properties";


	protected final File rootFolder;


	protected final Map<String, String> formatToExtMap;


	protected final Set<String> videoFormats;


	protected final Set<String> imageFormats;


	protected final Log log;


	/**
	 * The default constructor
	 */
	public AxesContentManager() {
		final Map<String, String> properties = PropertiesLoader.loadProperties(AxesContentManager.PROPERTY_FILE, this.getClass());
		this.log = LogFactory.getLog(this.getClass());

		if (this.log.isTraceEnabled()) {
			this.log.trace(properties.toString());
		}

		final String root = System.getProperty(AxesContentManager.ROOT_FOLDER, properties.get(AxesContentManager.ROOT_FOLDER));
		if (root == null) {
			final String message = "'" + AxesContentManager.ROOT_FOLDER + "' property not found neither in System properties nor " + AxesContentManager.PROPERTY_FILE + ".";
			this.log.error(message);
			throw new WebLabUncheckedException(message);
		}
		this.rootFolder = new File(root);
		if (!this.rootFolder.isAbsolute()) {
			this.log.warn("The " + AxesContentManager.ROOT_FOLDER + " path " + root + " is not absolute.");
		}
		if (this.rootFolder.exists() && !this.rootFolder.isDirectory()) {
			final String message = "The rootFolder " + root + " exist but is not a directory.";
			this.log.error(message);
			throw new WebLabUncheckedException(message);
		}
		if (!this.rootFolder.exists() && !this.rootFolder.mkdirs()) {
			final String message = "The rootFolder " + root + " does not exist and cannot be created.";
			this.log.error(message);
			throw new WebLabUncheckedException(message);
		}

		final String video = properties.get("videoFormats");
		if (video == null) {
			this.log.warn("No videoFormats property found in " + AxesContentManager.PROPERTY_FILE + ".");
			this.videoFormats = Collections.emptySet();
		} else {
			final String videoNoSpace = video.replaceAll("\\s+", "");
			final String[] videoExts = videoNoSpace.split(",");
			this.videoFormats = Collections.unmodifiableSet(new HashSet<>(Arrays.asList(videoExts)));
		}

		final String image = properties.get("imageFormats");
		if (image == null) {
			this.log.warn("No imageFormats property found in " + AxesContentManager.PROPERTY_FILE + ".");
			this.imageFormats = Collections.emptySet();
		} else {
			final String imageNoSpace = image.replaceAll("\\s+", "");
			final String[] imageExts = imageNoSpace.split(",");
			this.imageFormats = Collections.unmodifiableSet(new HashSet<>(Arrays.asList(imageExts)));
		}

		final Set<String> formats = new HashSet<>(this.videoFormats);
		formats.addAll(this.imageFormats);

		final Map<String, String> map = new HashMap<>();
		for (final Entry<String, String> entry : properties.entrySet()) {
			if (!entry.getKey().startsWith("format.")) {
				continue;
			}
			final String ext = entry.getKey().replace("format.", "").replaceAll("\\s+", "");
			if (!formats.contains(ext)) {
				this.log.warn("The extension " + ext + " used for mime type " + entry.getValue() + " is neither in video nor audio formats.");
				continue;
			}
			map.put(entry.getValue().replaceAll("\\s+", ""), ext);
		}

		this.formatToExtMap = Collections.unmodifiableMap(map);
	}


	protected String getFirstValue(final Value<String> value, final String property, final String resUri) throws WebLabCheckedException {
		if (!value.hasValue()) {
			throw new WebLabCheckedException("The " + property + " is not annotated on " + resUri + ".");
		}
		if (value.size() > 1) {
			this.log.warn("More than one " + property + " annotation found on " + resUri + ". Using first one");
		}
		return value.firstTypedValue();
	}


	protected String getResourceFormat(final Resource res) throws WebLabCheckedException {
		final DublinCoreAnnotator dca = new DublinCoreAnnotator(res);
		final Value<String> formats = dca.readFormat();
		return this.getFirstValue(formats, "dc:format", res.getUri());
	}


	/**
	 * @param uri
	 *            The URI of the content as annotated in the document
	 * @return The abstract file path to the resource denoted by <code>uri</code>
	 * @throws WebLabCheckedException
	 *             If the URI is not valid
	 */
	public File getFilePath(final URI uri) throws WebLabCheckedException {
		final String[] uriParts = uri.toString().split(":");
		if (uriParts.length == 5) {
			return this.getVideoFile(uriParts[3], uriParts[4].split("\\.")[0], uriParts[4]);
		}
		if (uriParts.length == 7) {
			return this.getImageFile(uriParts[3], uriParts[4], uriParts[5], uriParts[6]);
		}
		final String message = "URI " + uri + " is not valid.";
		this.log.error(message);
		throw new WebLabCheckedException(message);
	}


	protected File getImageFile(final String collectionId, final String videoId, final String shotId, final String frameFile) {
		final File collectionFolder = new File(this.rootFolder, collectionId);
		final File videoFolder = new File(collectionFolder, videoId);
		final File shotFolder = new File(videoFolder, shotId);
		return new File(shotFolder, frameFile);
	}


	protected File getVideoFile(final String collectionId, final String videoId, final String videoFile) {
		final File collectionFolder = new File(this.rootFolder, collectionId);
		final File videoFolder = new File(collectionFolder, videoId);
		return new File(videoFolder, videoFile);
	}


	protected String getFormatExt(final Resource res) throws WebLabCheckedException {
		final String format = this.getResourceFormat(res);
		if (!this.formatToExtMap.containsKey(format)) {
			final String message = "Format " + format + " not mapped to an extension.";
			this.log.error(message);
			throw new WebLabCheckedException(message);
		}
		return this.formatToExtMap.get(format);
	}


	/**
	 * @param resource
	 *            The resource on which to read the collection and video id (as well has shot and frame id in case of image).
	 * @param extension
	 *            The extension of the file to generate an URI for
	 * @return The URI that wil be use to refer the content associated to that particular resource
	 * @throws WebLabCheckedException
	 *             If extension is not handled or if resource is missing one of the required id
	 */
	public URI getContentUri(final Resource resource, final String extension) throws WebLabCheckedException {
		final AxesAnnotator annotator = new AxesAnnotator(resource);
		final String collectionId = NamingSchemeUtils.getCollectionId(annotator, resource);
		final String videoId = NamingSchemeUtils.getVideoId(annotator, resource);

		final URI contentId;
		if (this.videoFormats.contains(extension)) {
			contentId = NamingSchemeUtils.getVideoResourceURI(collectionId, videoId, extension);
		} else if (this.imageFormats.contains(extension)) {
			final String frameId = NamingSchemeUtils.getFrameId(annotator, resource);
			final String shotId = NamingSchemeUtils.getShotId(annotator, resource);
			contentId = NamingSchemeUtils.getImageResourceURI(collectionId, videoId, extension, shotId, frameId);
		} else {
			final String message = "Extension " + extension + " is not taken into account.";
			this.log.error(message);
			throw new WebLabCheckedException(message);
		}
		return contentId;
	}


	/**
	 * @return The map of mime types per extension
	 */
	public Map<String, String> getFormatToExtMap() {
		return this.formatToExtMap;
	}

}
