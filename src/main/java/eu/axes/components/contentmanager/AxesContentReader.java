package eu.axes.components.contentmanager;

import java.io.File;
import java.net.URI;

import org.ow2.weblab.content.api.ContentReader;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;

/**
 * This class get the file associated to an URI and check its existence.
 *
 * @author ymombrun
 * @date 2012-05-15
 */
public class AxesContentReader extends AxesContentManager implements ContentReader {


	/**
	 * The default constructor
	 */
	public AxesContentReader() {
		super();
		this.log.debug("AxesContentReader loaded with " + this.rootFolder + " as root folder.");
	}


	@Override
	public File readContent(final URI destUri) throws WebLabCheckedException {
		final File content = this.getFilePath(destUri);
		if (!content.exists()) {
			final String message = "File [" + content.getPath() + "] does not exists.";
			this.log.error(message);
			throw new WebLabCheckedException(message);
		}
		if (!content.isFile()) {
			final String message = "File [" + content.getPath() + "] is not a file.";
			this.log.error(message);
			throw new WebLabCheckedException(message);
		}
		if (!content.canRead()) {
			final String message = "File [" + content.getPath() + "] is not readable.";
			this.log.error(message);
			throw new WebLabCheckedException(message);
		}
		return content;
	}

}
