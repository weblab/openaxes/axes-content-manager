package eu.axes.components.contentmanager;

import java.io.InputStream;
import java.net.URI;

import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.content.api.ContentWriter;
import org.ow2.weblab.core.extended.exception.WebLabNotYetImplementedException;
import org.ow2.weblab.core.model.Resource;

/**
 * This class is just an writer that does noting.
 * It is just here to prevent from loading a FileContentManager writer when it is not needed.
 *
 * @author ymombrun
 * @date 2012-05-15
 */
public final class DevNullWriter implements ContentWriter {


	private static final String MESSAGE = "The implementation of the ContentWriter loaded does nothing.";


	@Override
	public URI writeContent(final InputStream content, final Resource resource) {
		LogFactory.getLog(this.getClass()).error(DevNullWriter.MESSAGE);
		throw new WebLabNotYetImplementedException(DevNullWriter.MESSAGE);
	}


	@Override
	public URI writeExposedContent(final InputStream content, final Resource resource) {
		LogFactory.getLog(this.getClass()).error(DevNullWriter.MESSAGE);
		throw new WebLabNotYetImplementedException(DevNullWriter.MESSAGE);
	}

}
