package eu.axes.components.contentmanager;

import java.io.File;
import java.net.URI;

import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.content.api.ContentReader;
import org.ow2.weblab.core.extended.exception.WebLabNotYetImplementedException;

/**
 * This class is just a reader that does noting.
 * It is just here to prevent from loading a FileContentManager reader when it is not needed.
 *
 * @author ymombrun
 * @date 2012-05-15
 */
public final class DevNullReader implements ContentReader {


	private static final String MESSAGE = "The implementation of the ContentReader loaded does nothing.";


	@Override
	public File readContent(final URI destUri) {
		LogFactory.getLog(this.getClass()).error(DevNullReader.MESSAGE);
		throw new WebLabNotYetImplementedException(DevNullReader.MESSAGE);
	}

}
